<?php
/**
 * Created by Vanya Medenko.
 * User: Иван
 * Date: 10.11.2018
 * Time: 14:42
 */
class ParserClass
{
    var $page;

    /**
     *
     * Метод для получения исходного HTML кода
     *
     * Принимаем ссылку и заносим в переменную link
     * Получаем исходный код с помощью File_get_content
     *
     * @ $link Переменная с ссылкой
     * @ $page Переменная с исходным HTML кодом
     */
    public function getContent(){
        $link = $_POST['link'];
        $this->page = file_get_contents($link);
    }

    /**
     *
     * Метод для получения цены
     *
     * Принимаем с помощью this исходный код, и извлекаем цену
     * с помощью регулярного выражения
     * Выводим цену
     *
     * @ $price Переменная цены
     */
    public function getPrice(){
        preg_match_all('~<span\s+itemprop="price"\s+content="(.*?)">(.*?)</span>~s', $this->page, $match);
        $price = $match[2][0];
        print_r($price);
    }
    /**
     *
     * Метод для получения заголовка
     *
     * Принимаем с помощью this исходный код, и извлекаем заголовок
     * с помощью регулярного выражения
     * Выводим заголовок
     *
     * @ $title Переменная заголовка
     */
    public function getTitle(){
        preg_match_all('~<h1[^>]*>(.+?)</h1>~s', $this->page, $match);
        $title = $match[1][0];
        print_r($title);
    }
    /**
     *
     * Метод для получения картинки
     *
     * Принимаем с помощью this исходный код, и извлекаем ссылку картинки
     * с помощью регулярного выражения
     * Выводим ссылку
     *
     * @ $image Переменная ссылки картинки
     */
    public function getImage(){
        preg_match_all('~<img\s+src="(.*?)"\s+id="mainImage"\s+alt="(.*?)"\s+itemprop="image"\s*?/>~', $this->page, $match);
        $image = $match[1][0];
        print_r($image);
    }
}
