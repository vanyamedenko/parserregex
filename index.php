<?php
/**
 * Created by Vanya Medenko.
 * User: Иван
 * Date: 09.11.2018
 * Time: 18:07
 */

include 'ParserClass.php';
/**
 * Объект класса
 *
 * Используем для обращений к функциям класса
 */
$obj = new ParserClass();

/**
 * Форма для ввода и отправки данных.
 *
 * Данные отправляются в класс, что бы парсить
 * необходимую информацию с исходной ссылки
 */
echo"<form action='index.php' method='post'>
    <input type='text' name='link' placeholder='Enter your link'>
    <input type='submit' name='search' value='Search'>
    </form>";

/**
 * Блок вызова методов класса
 *
 * Используем для вывода результата парсинга
 * Вызываем методы класса с необходимой информацией
 */
if(isset($_POST['search'])){
    $obj->getContent();
    echo '<br>';
    $obj->getTitle();
    echo '<br>';
    $obj-> getPrice();
    echo '<br>';
    $obj-> getImage();
}
